# SprintSquads TemplateEngine v1.1

## About
В данном маркдауне будет описано как работает шаблонизатор. Изначально шаблонизатор был создан с целью заведения более **200** разнотипных платжей на 3 фронтах(**iOS/Android/Web**), однако архитектурно на основе логики шаблонизатора в можно написать большую часть однотипных экранов, и даже маленькую игру с ответами на вопросы (мы пробовали, компонентов не хватило). 

С помощью этой **коробки** можно минимизировать время которое тратит разработчик для создания сервисов платежей а в будущем возможно и любых страниц в целом, если дописать пару вещей.

## Архитектура платежей и переходов

1. Когда пользователь нажимает на таб платежей происходит запрос на 
``` 
GET: https://testapi2.homebank.kz/homebank-api/api/v2/services
```
HB API собирает данные из баз в Core и Dictionaries и собирает фронту ответ.
Приходит массив примерно таких обьектов:
```
[{
    "id": 5964,
    "name": "rudnyiVodokanal",
    "providerId": 140,
    "description": {
      "rus": "Рудненский водоканал",
      "eng": "Рудненский водоканал",
      "kaz": "Рудненский водоканал"
    },
    "categories": [
      3
    ],
    "countries": [
      85
    ],
    "regions": [
      11
    ],
    "commission": {
      "type": "static",
      "info": [
        {
          "value": 0.0
        }
      ]
    },
    "isSuspended": false,
    "isVisible": true,
    "isCustom": false,
    "hasDetails": false,
    "hasReceipt": false,
    "hasTemplateView": true
  }]
```
Для того чтобы понять нужно ли нам показывать сервис в общем списке доступных сервисов важны значения  ```isVisible, isSuspended, isCustom, hasTemplateView```, а так же есть хардкодная привязка к ```providerId```

Для того чтобы понять в какой город и категорию определить сервис мы используем массивы: ```categories, countries, regions```

Комиссия по сервису всегда приходит с бэка, в теории может быть и не статичной, однако таких сервисов пока нет.
```
"commission": {
      "type": "static",
      "info": [
        {
          "value": 0.0
        }
      ]
    }
```
2. На фронтах есть логика раутинга по сервисам(какие экраны и в каких случаях стоит открывать), и к сожалению пока что не все сервисы переведены на текущий шаблонизатор. По этому мы смотрим на значениее ```hasTemplateView``` и если оно true то мы длаеем запрос на шаблон, не забудьте заменить serviceName и вставить Bearer если тестите с инсомнии или постмана:
``` 
GET: https://testapi2.homebank.kz/homebank-api/api/v1/services/{serviceName}/template?engineVersion=1.2
```
Бэкенд проводит операции и смотрит на пачку настроек сервиса и динамично генерирует сам шаблон. На фронт приходит ответ, это уже и есть сам шаблон. Ответ выглядит так:
```
{"id":"aparuTaxi","options":{"subTitle":{"ru-RU":"Оплата услуг сервиса","kz-KZ":"Сервис қызметтеріне ақы төлеу","en-US":"Pyament for the services provided"},"canChooseBonus":false,"webStyles":[{"key":"background-image","value":"linear-gradient(103deg,#10a9ff,#005a8b)"}],"startingPageId":"initialPage"},"pages":[{"id":"initialPage","type":"normal","options":{},"containers":[{"id":"0","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":0,"right":0,"top":0,"bottom":0}},"blocks":[{"id":"0-0","type":"roundedContainer","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":16,"right":16,"top":16,"bottom":0}},"elements":[{"id":"0-0-0","parameterId":"contract","type":"inputField","options":{"mobileDefaultConstraints":{"left":16,"right":16,"top":2,"bottom":2},"description":{"ru-RU":"Номер лицевого счета","kz-KZ":"Шот нөмірі","en-US":"Account number"},"webPlaceholder":{"ru-RU":"0000123","kz-KZ":"0000123","en-US":"0000123"},"formatting":"number","keyboard":"number","onResignPreCheck":{"parameterId":"contract","checkCondition":{"regex":"^[0-9]{3,}$"},"errorActions":[{"id":"rpc-0.0","type":"highlightElem","options":{"targetElem":"0-0-0","highlightParent":true,"text":{"ru-RU":"Введите корректный номер счета","kz-KZ":"Шоттың нөмірін енгізіңіз","en-US":"Please enter an account number"}}}]}}}]},{"id":"0-1","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":16,"right":16,"top":8,"bottom":0}},"elements":[{"id":"0-1-0","type":"label","options":{"webTextAlignment":"left","mobileTextAlignment":"left","mobileDefaultConstraints":{"left":0,"right":0,"top":0,"bottom":0},"description":{"ru-RU":"Указан на бумажной квитанции","kz-KZ":"Қағаз түбіртегінде көрсетілген","en-US":"Indicated on paper receipt"},"icon":"template_label_info","fontSize":13}}]},{"id":"0-2","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":0,"right":0,"top":16,"bottom":80}},"elements":[{"id":"0-2-0","type":"submit","parameterId":"continue","actionId":"checkContract","options":{"webTextAligment":"center","mobileDefaultConstraints":{"left":16,"right":16,"top":16,"bottom":0},"description":{"ru-RU":"Проверить","kz-KZ":"Тексеру","en-US":"Check"}}}]}]}]},{"id":"secondPage","type":"normal","options":{},"containers":[{"id":"1","type":"flat","options":{"mobileDefaultConstraints":{"left":0,"right":0,"top":0,"bottom":0}},"blocks":[{"id":"1-0","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":0,"right":0,"top":16,"bottom":0},"mobileBackgroundColor":"FFFFFF"},"elements":[{"id":"1-0-0","type":"flatList","options":{"showUnderline":true,"mobileDefaultConstraints":{"left":16,"right":16,"top":8,"bottom":0},"description":{"ru-RU":"Информация","kz-KZ":"Ақпарат","en-US":"Information"},"listData":[{"id":"1-0-0-0","label":{"ru-RU":"Номер лицевого счета","kz-KZ":"Шот нөмірі","en-US":"Account number"},"value":"{0}","dependencies":["contract"]},{"id":"1-0-0-1","label":{"ru-RU":"ФИО","kz-KZ":"Аты-жөні","en-US":"Account holder name"},"value":"{0}","dependencies":["clientName"]},{"id":"1-0-0-2","label":{"ru-RU":"Адрес","kz-KZ":"Мекен-жайы","en-US":"Account address"},"value":"{0}","dependencies":["clientAddress"]},{"id":"1-0-0-3","label":{"ru-RU":"Информация","kz-KZ":"Информация","en-US":"Information"},"value":"{0}","dependencies":["additionalInfo"]}]}},{"id":"1-0-1","type":"getInfo","parameterId":"info","actionId":"hideAllErrors","options":{"mobileDefaultConstraints":{"left":16,"right":16,"top":2,"bottom":2},"description":{"ru-RU":"Баланс","kz-KZ":"Баланс","en-US":"Balance"},"buttonText":{"ru-RU":"Проверить","kz-KZ":"Тексеру","en-US":"Check"},"debtText":{"ru-RU":"Задолженность на {hour:minute}","kz-KZ":"{hour:minute}-да Қарыз","en-US":"Debt on {hour:minute}"},"positiveText":{"ru-RU":"Переплата на {hour:minute}","kz-KZ":"Переплата на {hour:minute}","en-US":"Balance on {hour:minute}"}}}]},{"id":"1-1","type":"roundedContainer","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":16,"right":16,"top":16,"bottom":0}},"elements":[{"id":"1-1-0","parameterId":"amount","type":"inputField","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":16,"right":16,"top":2,"bottom":2},"description":{"ru-RU":"Сумма к оплате","kz-KZ":"Төленетін сома","en-US":"Amount to pay"},"webPlaceholder":{"ru-RU":"200","kz-KZ":"200","en-US":"200"},"formatting":"priceAmount","keyboard":"decimal","onResignPreCheck":{"parameterId":"amount","checkCondition":{"range":{"min":"1","max":"1000000000","dataType":"double"}},"errorActions":[{"id":"rpc-1.0","type":"highlightElem","options":{"targetElem":"1-1-0","highlightParent":true,"text":{"ru-RU":"Введите сумму","kz-KZ":"Соманы енгізіңіз","en-US":"Please enter the amount"}}}]}}}]},{"id":"1-2","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":0,"right":0,"top":16,"bottom":0}},"elements":[{"id":"1-2-0","parameterId":"commission","actionId":"commissionAlert","type":"commission","options":{"calculateFromParamId":"amount","webTextAligment":"left","mobileTextAlignment":"center","mobileDefaultConstraints":{"left":0,"right":0,"top":0,"bottom":0},"description":{"ru-RU":"Комиссия: {0} ₸","kz-KZ":"Комиссия: {0} ₸","en-US":"commission: {0} ₸"},"highLightedText":[{"ru-RU":"{0} ₸","kz-KZ":"{0} ₸","en-US":"{0} ₸"}],"dependencies":["commission"]}}]},{"id":"1-3","type":"roundedContainer","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":16,"right":16,"top":16,"bottom":0}},"elements":[{"id":"1-3-0","parameterId":"card","type":"paymentSelector","options":{"mobileDefaultConstraints":{"left":16,"right":16,"top":2,"bottom":2},"label":{"ru-RU":"Выберите способ оплаты","kz-KZ":"Төлем әдісін таңдаңыз","en-US":"Choose a payment method"}}}]},{"id":"1-4","type":"flat","options":{"webAlignment":"Vertical","mobileDefaultConstraints":{"left":0,"right":0,"top":16,"bottom":80}},"elements":[{"id":"1-4-0","type":"submit","actionId":"showExecPage","parameterId":"overallAmount","options":{"webTextAligment":"center","mobileDefaultConstraints":{"left":16,"right":16,"top":16,"bottom":0},"description":{"ru-RU":"Оплатить {0} ₸","kz-KZ":"Төлеу үшін {0} ₸","en-US":"Pay {0} ₸"},"dependencies":["overallAmount"],"sumFromParams":["amount","commission"]}}]}]}]},{"id":"execPage","type":"execPage","options":{"actionId":"exec","displayInfo":{"card":"card","commission":"commission","amount":"amount"}},"containers":[]}],"parameters":[{"id":"contract","dataType":"string"},{"id":"continue","dataType":"double"},{"id":"clientName","dataType":"string"},{"id":"clientAddress","dataType":"string"},{"id":"additionalInfo","dataType":"string"},{"id":"amount","dataType":"double"},{"id":"info","dataType":"double"},{"id":"card","dataType":"cardData"},{"id":"overallAmount","dataType":"double"},{"id":"commission","dataType":"double"},{"id":"errorDesc","dataType":"string"}],"actions":[{"id":"hideAllErrors","type":"hideAllErrors","options":{}},{"id":"commissionAlert","type":"alert","options":{"text":{"ru-RU":"Комиссия Homebank 0 ₸ \nКомиссия агрегатора {0} ₸","kz-KZ":"Homebank комиссиясы 0 ₸ \nАгрегатордың комиссиясы {0} ₸","en-US":"Homebank Commission 0 ₸ \nAggregator commission {0} ₸"},"title":{"ru-RU":"Комиссия","kz-KZ":"Комиссия","en-US":"Commission"},"dependencies":["commission"]}},{"id":"checkContract","type":"checkRequest","options":{"parameters":[{"id":"contract","valueFromParam":"contract"},{"id":"amount","value":"10"}],"preChecks":[{"parameterId":"contract","checkCondition":{"regex":"^[0-9]{3,}$"},"errorActions":[{"id":"pc-0.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"continue"}},{"id":"pc-0.1","type":"highlightElem","options":{"targetElem":"0-0-0","highlightParent":true,"text":{"ru-RU":"Введите корректный номер счета","kz-KZ":"Шот нөмірін дұрыс енгізіңіз","en-US":"Please enter a correct account"}}},{"id":"pc-0.2","type":"becomeFirstResponder","options":{"targetElem":"0-0-0"}},{"id":"pc-0.3","type":"taptic","options":{"tapType":"warning"}}]}],"errorResponseActions":[{"code":"-214","responseActions":[{"id":"era-0.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"continue"}},{"id":"era-0.1","type":"highlightElem","options":{"targetElem":"0-0-0","highlightParent":true,"text":{"ru-RU":"Данный абонент не найден","kz-KZ":"Бұл шот табылмады","en-US":"This account was not found"}}},{"id":"era-0.2","type":"becomeFirstResponder","options":{"targetElem":"0-0-0"}},{"id":"era-0.3","type":"taptic","options":{"tapType":"warning"}}]},{"code":"other","responseActions":[{"id":"era-1.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"continue"}},{"id":"era-1.1","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"resultDescription","dataType":"string"}],"parameterId":"errorDesc"}},{"id":"era-1.2","type":"alert","options":{"text":{"ru-RU":"{0}","kz-KZ":"{0}","en-US":"{0}"},"title":{"ru-RU":"Ошибка","kz-KZ":"Қате","en-US":"Error"},"dependencies":["errorDesc"]}},{"id":"era-1.3","type":"taptic","options":{"tapType":"warning"}}]}],"successResponseActions":[{"id":"sra-0.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"continue"}},{"id":"sra-0.1","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"clientName","dataType":"string"}],"parameterId":"clientName"}},{"id":"sra-0.2","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"clientAddress","dataType":"string"}],"parameterId":"clientAddress"}},{"id":"sra-0.3","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"displayInfo","dataType":"string"}],"parameterId":"additionalInfo"}},{"id":"sra-0.4","type":"setParam","options":{"dataType":"double","valueFrom":[{"id":"balance","dataType":"double"}],"parameterId":"amount"}},{"id":"sra-0.5","type":"setParam","options":{"dataType":"double","valueFrom":[{"id":"balance","dataType":"double"}],"parameterId":"info","inverse":true}},{"id":"sra-0.6","type":"showPage","options":{"pageId":"secondPage"}}]}},{"id":"showExecPage","type":"checkRequest","options":{"parameters":[{"id":"contract","valueFromParam":"contract"},{"id":"amount","valueFromParam":"amount"}],"preChecks":[{"parameterId":"amount","checkCondition":{"range":{"min":"1","max":"1000000000000","dataType":"double"}},"errorActions":[{"id":"pc-1.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"overallAmount"}},{"id":"pc-1.1","type":"highlightElem","options":{"targetElem":"1-1-0","highlightParent":true,"text":{"ru-RU":"Введите корректную сумму","kz-KZ":"Дұрыс соманы енгізіңіз","en-US":"Please enter an amount"}}},{"id":"pc-1.2","type":"becomeFirstResponder","options":{"targetElem":"1-1-0"}},{"id":"pc-1.3","type":"taptic","options":{"tapType":"warning"}}]},{"parameterId":"card","errorActions":[{"id":"pc-2.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"overallAmount"}},{"id":"pc-2.1","type":"highlightElem","options":{"targetElem":"1-3-0","highlightParent":true}},{"id":"pc-2.2","type":"becomeFirstResponder","options":{"targetElem":"1-3-0"}},{"id":"pc-2.3","type":"taptic","options":{"tapType":"warning"}}]}],"errorResponseActions":[{"code":"other","responseActions":[{"id":"era-2.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"overallAmount"}},{"id":"era-2.1","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"resultDescription","dataType":"string"}],"parameterId":"errorDesc"}},{"id":"era-2.2","type":"alert","options":{"text":{"ru-RU":"{0}","kz-KZ":"{0}","en-US":"{0}"},"title":{"ru-RU":"Ошибка","kz-KZ":"Қате","en-US":"Error"},"dependencies":["errorDesc"]}},{"id":"era-2.3","type":"taptic","options":{"tapType":"warning"}}]}],"successResponseActions":[{"id":"sra-1","type":"showPage","options":{"pageId":"execPage"}}]}},{"id":"exec","type":"execRequest","options":{"chosenCardParam":"card","parameters":[{"id":"amount","valueFromParam":"amount"}],"errorResponseActions":[{"code":"other","responseActions":[{"id":"era-3.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"overallAmount"}},{"id":"era-3.1","type":"setParam","options":{"dataType":"string","valueFrom":[{"id":"resultDescription","dataType":"string"}],"parameterId":"errorDesc"}},{"id":"era-3.2","type":"alert","options":{"text":{"ru-RU":"{0}","kz-KZ":"{0}","en-US":"{0}"},"title":{"ru-RU":"Ошибка","kz-KZ":"Қате","en-US":"Error"},"dependencies":["errorDesc"]}},{"id":"era-3.3","type":"taptic","options":{"tapType":"warning"}}]}],"successResponseActions":[{"id":"sra-2.0","type":"setParam","options":{"dataType":"double","value":null,"parameterId":"overallAmount"}},{"id":"sra-2.1","type":"finishTemplate","options":{"title":{"ru-RU":"Спасибо","kz-KZ":"Рахмет","en-US":"Thanks"},"text":{"ru-RU":"Ваш платеж успешно отправлен в обработку!","kz-KZ":"Сіздің төлеміңіз сәтті өңдеуге жіберілді!","en-US":"Your payment has been successfully sent for processing!"}}}]}}]}
```
>Это jsonMinify версия ответа.
3. TemplateEngine(далее движок) принимает шаблон и обрабатывает этот json в экраны и инструкции к этим экранам. При отрисвоке экранов движок может лишь распозновать знакомые ему компоненты и выполнять действия которые ему знакомы. Снизу мы видим как движок обрабатывал данный json и отрисовывал экран и определил внутресервисную логику.

 ![Пример на iOS](https://sun9-36.userapi.com/Q0DAJBBuNzt6rH07-pbhy0JgSXT-yksu3buqaw/_wK8MVXRLSo.jpg =375x)


4. Пользователь вводит нужные ему данные и шаблонизатор лишь применяет поведенчиские признаки которые были описаны в json. Пользователь совершает оплату и выходит на главный экран. 🤺 

### Общая диаграмма взаимодействий

Обычно схема взаимодействий для оплаты сервиса на шаблонном движке выглядят примерно так, очередность действий сохранена, но может быть изменена в зависимости от действий пользователя. Ниже рассматривается дефолтный кейс, незначительные вещи могли быть упущены.

Это [Mermaid](https://mermaidjs.github.io/) Uml viewer.

---
```mermaid
sequenceDiagram

3 Клиента ->> HB Back: Запрос на весь список сервисов

Note right of 3 Клиента: Если у сервиса<br/>"hasTemplateView": true <br/>идет запрос на<br/> шаблон в /template

3 Клиента ->> HB Back: Запрос для получения шаблона
HB Back ->> HB Back: Проверка настроек сервиса и генерация шаблона
Note right of 3 Клиента: Пример:  если сервис<br/>настроен с бонусами<br/>canpaybybnous<br/> в options сервиса<br/> приходит true
HB Back ->> 3 Клиента: Шаблон для движка

alt Info step (optional)
3 Клиента -->> HB Back: info запрос для получения доп данных(баланс/фио)
HB Back -->> ПШ: Запрос для получения  данных по контракту X для сервиса Y
ПШ -->> ПШ: Если сервис оффлайновый, проверка на данные
ПШ -->> Онлайн Поставщик Y: Запрос для получения данных по контракту X
Онлайн Поставщик Y -->> ПШ: Ответ с данными
ПШ -->> ПШ: Стандартизация данных
ПШ -->> HB Back: Ответ с данными в XML
HB Back -->> HB Back: Стандартизация данных
HB Back -->> 3 Клиента: Ответ с данными
end
alt Check step
3 Клиента ->> HB Back: check запрос (POST: /payments)
HB Back ->> ПШ: Запрос check для проверки существования данных по контракту X для сервиса Y
ПШ -->> ПШ: Если сервис оффлайновый, проверка на данные
ПШ -->> Онлайн Поставщик Y: Запрос для получения данных по контракту X
Онлайн Поставщик Y ->> ПШ: Ответ с данными
ПШ ->> ПШ: Стандартизация данных
ПШ ->> HB Back: Ответ с данными и processID
HB Back ->> HB Back: Стандартизация данных
HB Back ->> 3 Клиента: Ответ с данными и paymentID
Note right of 3 Клиента: Если сервис<br/>настроен с otp<br/>приходит authorizationType<br/> в ответе на чек
end
alt Pay step
3 Клиента -->> HB Back: Запрос на подтверждение OTP
3 Клиента ->> HB Back: exec запрос (PUT: /payments)
HB Back ->> HB Back: Попытка Фриза средств
HB Back -->> 3 Клиента: ошибка если фриз не удался
HB Back ->> ПШ: Запрос PAY для проверки существования по контракту X для сервиса Y и Payment ID
ПШ -->> Онлайн Поставщик Y: Запрос на оплату по контракту X
Онлайн Поставщик Y ->> ПШ: Ответ с об успехе или об ошибке
ПШ ->> ПШ: Стандартизация данных
ПШ ->> HB Back: Ответ с успехом или ошибкой и processID
HB Back ->> HB Back: Стандартизация данных
HB Back -->> HB Back: Отмена фриза при неудаче с пш
HB Back ->> HB Back: Списание средств при успехе
HB Back ->> 3 Клиента: Ответ со статусом платежа и paymentID
3 Клиента -->> 3 Клиента: Отображение ошбики при ошибке
3 Клиента ->> 3 Клиента: Закрытие экрана платежей при успехе
end
```
---
### Статусы ошибок и версионность движка

Помимо успешного ответа **200**, может так же вернуться ответ со статусом **404** или **426** ошибки.

**404** ошибка означает что HB API не смог найти настройки для вызванного сервиса чтобы сформировать шаблон или не смог найти статичный шаблон для этого сервиса, если вы видите это на продакшне, то нужно обратиться к ребятам с бэкенда, что-то не так.

**426** ошибка означает что версия шаблонизатора устарела и бэкенд не имеет шаблона для отправленной версии шаблонизатора. На момент 18.02.2019 существуют две версии шаблонизатора **1.0 и 1.1**.

Версионность шаблонизатора существует для того чтобы описывать какие компоненты и действия может обрабатывать движок той версии, и с какая логика стоит под капотом. Представим что Шаблонизатор версии один лишь знает что такое ```inputField и button```. Мы выкатываем это на продкашн с версией движка 1.0. Фронт стучится на бэк за шаблоном для сервиса ```SSquadsPay``` с версией 1.0 - бэк смотрит хватает ли Компонентов и действий доступных для 1.0( ```inputfield/button```) для того чтобы отрисовать странцу для ```SSquadsPay```. Если по логике сервис простой, возможно этого и хватит и он вернет 200 и сам шаблон. Однако Представим что после того как все было выпущено на продкашн определенный поставщик хочет себе вместо обычногоо инпутфилда с цифрами, какой нибудь барабан.

После аппрува дизайнера, разработчики 3 фронтов создают компонент и называют его ```barabanchik```  и выкатывают релиз с версией шаблонизатора 1.1. Новый клиент стучится в бэк, однако так как работы еще не были проведены на backend, и сам шаблон не был обновлен( бэк еще не знает что такое ```barabanchik```) бэкенд возвращает прежний шаблон версии 1.0, который все еще работает даже для   1.1 потому что ```(это важно)``` все новые версии движка должны уметь обрабатывать более устаревшие версии шаблона. Далее бэкенд создает еще один инстанс шаблона версии 1.1 для сервиса ```SSquadsPay``` где использует ```barabanchik```, таким образом для старых клиентов которые еще не обновились приходит шаблон версии 1.0 с ```inputField``` где пользователь просто вводит данные, а те пользователи что обновились уже крутят ```barabanchik```. 

Теперь представим что в халык звонят поставщики сервиса и говорят: ```мы ненавидим инпутфилды потому что пользователи вводят что попало и нам приходят много запросов, оставьте только барабан```. В таком случае, происходит удаление инстанса шаблона 1.0 на бэке и теперь когда пользователи которые сидят на старых клиентах запрашивают шаблон для сервиса ```SSquadsPay``` им приходит 426 ошибка. На клиентах происходит попап с сообщением мол: "Данный сервис доступен в обновленной версии приложения хоумбанка пожалуйста обновитесь". Пользователь обновляет приложение, где версия движка 1.1 и сервис работатет корректно так как на бэкенд уже идет запрос где engineVersion=1.1
``` 
GET: https://testapi2.homebank.kz/homebank-api/api/v1/services/SSquadsPay/template?engineVersion=1.1
```

Общую логику можно проследить в этой схеме:

```mermaid
graph LR
A[Клиент] -- Запрос на шаблон сервиса с описанием версии движка фронта --> B((HP API))
B --> D(Проверка на существование сервиса)
D --> F(Проверка на Версионность)
D -- Нет какой-либо версии шаблона для этого сервиса: 404 --> A
F -- Нашелся шаблон,который подходит минимальным требованиям движка, возвращает статус 200 и сам шаблон --> A[Клиент]
F -- Нашелся шаблон, но версия движка на клиенте устарела, возвращает ошибку 426 --> A[Клиент]
```



## Структура шаблона версии 1.0-1.1

Каждый шаблон можно разделить на структуры, каждая из которой ответственна за собственный функционал. 


```mermaid
graph LR

1(Массив)
Переменная
4{Переменная, enum}
3((Обьект))
A>Возможное значение для enum]

```
Структура выглядит так:
```mermaid
graph LR
Шаблон((Шаблон)) -- shortname сервиса--> serviceName[id]
Шаблон((Шаблон)) -- настройки сервиса и шаблона--> 2((options))
Шаблон((Шаблон)) -- страницы для прорисовки сервиса--> 3(pages)
Шаблон((Шаблон)) -- переменные которые используются в сервисе--> 4(parameters)
Шаблон((Шаблон)) -- действия которые существуют в сервисе: чек,пей,</br>переходы между страницами, ошибки итд --> 5(actions)
4(parameters)--> id
4(parameters)--> datatype{dataType}
4(parameters)--> initialValue
datatype{dataType} --> string>string]
datatype{dataType} --> bool>bool]
datatype{dataType} --> int>int]
datatype{dataType} --> double>double]
datatype{dataType} --Обьект карты--> cardData>cardData]
datatype{dataType} --> dict>dict]
datatype{dataType} --> array>array]
2((options)) --> startingPageId
2((options)) --> subTitle
subTitle((subTitle)) --наследует обьект--> text((text))
2((options)) --массив --> webStyles(webStyles)
2((options)) --> canPayByBonus{canPayByBonus}
canPayByBonus{canPayByBonus} --> false>false]
canPayByBonus{canPayByBonus} --> true>true]
text((text)) --> ru-RU
text((text)) --> en-US
text((text)) --> kz-KZ
webStyles(webStyles) --> key
webStyles(webStyles) --> value 
3(pages) --> pageId[id]
startingPageId --Ссылается на--> pageId[id]
3(pages) --> pageType{pageType}
3(pages) --> pageOptions((options))
3(pages) --> containers(containers)
pageType{pageType} --> normal>normal]
pageType{pageType} --> execPage>execPage]
pageType{pageType} --> noInvoicePage>noInvoicePage]
pageOptions((options)) --> initActions(initActions)
initActions(initActions) --запускает при открытии--> actionId[id]
pageOptions((options)) --> mobileBackgroundColor
pageOptions((options)) --> displayInfo((displayInfo))
pageOptions((options)) --> pageactionId[actionId]
execPage>execPage] --действие при оплате--> pageactionId[actionId]
pageactionId[actionId] --> actionId[id]
5(actions) --> actionId[id]
displayInfo((displayInfo)) --> dcontract[contract]
displayInfo((displayInfo)) --> damount[amount]
displayInfo((displayInfo)) --> dcard[card]
displayInfo((displayInfo)) --> dcommission[commission]
execPage>execPage] --использует--> damount[amount]
execPage>execPage] --использует--> dcard[dcard]
execPage>execPage] --использует--> dcommission[commission]
noInvoicePage>noInvoicePage] --использует--> dcontract[сontract]
containers(containers) --> containers(containers)
containers(containers) --> contId[id]
containers(containers) --> containertype{type}
containers(containers) --> containerOptions((options))
containers(containers) --> elements(elements)
**var**  id: String!

**var**  type: ContainerType!

**var**  options: ContainerOptions!

**var** blocks: [Container]?

**var** elements: [Element]?
```
# options
Настройки самого шаблона и сервиса. Тут можно засетить стили для веба, обозначить можно ли оплачивать данный сервис бонусами и указать с какой страницы стоит начать инициализациую шаблона.

Пример options для сервиса Х:
```json
"options": {
    "subTitle": {
      "ru-RU": "Оплата услуг сервиса",
      "kz-KZ": "Сервис қызметтеріне ақы төлеу",
      "en-US": "Pyament for the services provided"
    },
    "webStyles": [
      {
        "key": "background-image",
        "value": "linear-gradient(103deg,#22C3B1,#6BB0E2)"
      }
    ],
    "startingPageId": "initialPage",
    "canPayByBonus": true
  }
```
Состоит из данных частей:

## startingPageId
Указывает templateEngine какой page показать первым.
```json
"startingPageId": "initialPage"
```
Сеттинг стартовой страницы
```swift
let currentPage = template.pages?.first(where: { (page) -> Bool in
page.id == template.options.startingPageId
})
```
Прорисовка страницы через **initialize**
```swift
private  func  initialize() {
if let p = state.currentPage {
  drawPage.onNext(p)
  }
  if  state.template.options.startingPageId  !=  state.currentPageId {
    showQuitTemplateButton.onNext(())
  }
}
```

## subTitle
>web only
>
Имеет такую же структуру как **text**.
Используется на странице как сабтайтл для описания сервиса.

```json
"subTitle": {
      "ru-RU": "Оплата услуг сервиса",
      "kz-KZ": "Сервис қызметтеріне ақы төлеу",
      "en-US": "Pyament for the services provided"
    }
```

## webStyles
Массив  css стилей которые в формате key/value которые имплементируются для страницы сервиса.

Можно добавить картинку локально и вместо градиена показать какую-то картинку, либо изменить градиент на необходимое значение.
>То что приходит в JSON
``` json
"webStyles": [
      {
        "key": "background-image",
        "value": "linear-gradient(103deg,#10a9ff,#005a8b)"
      }
    ]
```

> TypeScript Имплементация в коде
```typescript
getPaymentStyle() {
  let paymentStyle = {};
  this.responsePayment.options.webStyles.forEach(element => {
  paymentStyle[element.key] = element.value;
  });
  return paymentStyle;
}
```
>HTML
```html
<section class="top-section" [ngStyle]="getPaymentStyle()" *ngIf="dataLoaded">
```


## canPayByBonus
Описывает возможность выбора оплаты GO бонусами для сервиса. Настраивается в Core базе. Автогенерируется для всех шаблонов.

>Опциональное Bool значание. При отсутствии равно false.

```json
"canPayByBonus": true
```
Имплементация:
```swift
private var allCards: [AccountItemPickable] {
  return  state.template.options.canChooseBonus  ==  true ? bonusCards  +  paymentCards : paymentCards
}
```


# parameters
Массив переменных которые используются в шаблоне, туда можно записать значения через  ```setParam``` 
Компоненты используют параметры для сохранения определенных данных, и чтения их же, обычно используются привязки к изменению значений. Если параметр меняется а компонент к нему привязан, компонент перерисуется/изменит показываемое значение. Концептуально можно относиться к этому инстансу как к обычным переменным в коде. Можно задать изначальное значение для перменной как и через setParam так и через initalValue
>? -> Опциональная переменная
>! -> Обязательная переменная

Пример того что может прийтти в parameters
```json
"parameters": [
    {
      "id": "contract",
      "dataType": "string"
    },
    {
      "id": "continue",
      "dataType": "double"
    },
    {
      "id": "invoiceId",
      "dataType": "string"
    },
    {
      "id": "clientName",
      "dataType": "string"
    },
    {
      "id": "clientAddress",
      "dataType": "string"
    },
    {
      "id": "amount",
      "dataType": "double"
    },
    {
      "id": "invoices",
      "dataType": "array"
    },
    {
      "id": "invoiceItems",
      "dataType": "array"
    },
    {
      "id": "card",
      "dataType": "cardData"
    },
    {
      "id": "overallAmount",
      "dataType": "double"
    },
    {
      "id": "commission",
      "dataType": "double"
    },
    {
      "id": "errorDesc",
      "dataType": "string"
    }
  ]
```
```swift
struct  Parameter: Codable {
  var id: String!
  var dataType: DataType?
  var initialValue: String?
}
```

### id
>Обязательный параметр, это название переменной(параметра), по которой ее можно изменить через **setParam** или привязать компонент чтобы брал данные из этой переменной через **parameterId**
```swift
var id: String!
```

### initialValue
>Опционально. 
>
Можно задать изначальное значение для перменной. 

К примеру если вы хотите чтобы в шаблоне изначальная сумма вне зависимости от контракта была презабитыми 20 теньгами то можете передать в обьект param:
 >то что должно вернуться
```json
{
  "id": "amount",
  "dataType": "double",
  "initialValue": "20"
}
```

>Реализация в iOS
```swift
if let initialValue = parameter.initialValue {
                parameters[parameter.id] = initialValue
            }
```

### dataType
>Описание типов данных
>cardData это описание обьекта карты, пу сути dict.
```swift
switch dataType { 
  case .cardData, .dict: value = (value as? [String: AnyHashable]) ?? nil 
  case .double: value = convertToDouble(value: value)
  case .int: value = convertToInt(value: value)
  case .string: value = convertToString(value: value)   
  case .array: value = value as? [AnyHashable] ?? nil 
}
```

# pages
Массив страниц которые могут показываться внутри шаблона для того чтобы выполнить весь возможный функционал для оплаты сервиса. Одна страница может показываться в одновременно. Навигация происходит через action **showPage**.
>Пример страниц двухстраничного аккаунтового сервиса **astanaCityLift**
```json
"pages": [
    {
      "id": "initialPage",
      "type": "normal",
      "options": {},
      "containers": [
        {
          "id": "0",
          "type": "flat",
          "options": {
            "webAlignment": "Vertical",
            "mobileDefaultConstraints": {
              "left": 0,
              "right": 0,
              "top": 0,
              "bottom": 0
            }
          },
          "blocks": [
            {
              "id": "0-0",
              "type": "roundedContainer",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 16,
                  "right": 16,
                  "top": 16,
                  "bottom": 0
                }
              },
              "elements": [
                {
                  "id": "0-0-0",
                  "parameterId": "contract",
                  "type": "inputField",
                  "options": {
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 2,
                      "bottom": 2
                    },
                    "description": {
                      "ru-RU": "Номер лицевого счета",
                      "kz-KZ": "Шот нөмірі",
                      "en-US": "Account number"
                    },
                    "webPlaceholder": {
                      "ru-RU": "0000123",
                      "kz-KZ": "0000123",
                      "en-US": "0000123"
                    },
                    "formatting": "number",
                    "keyboard": "number",
                    "onResignPreCheck": {
                      "parameterId": "contract",
                      "checkCondition": {
                        "regex": "^[0-9]{3,}$"
                      },
                      "errorActions": [
                        {
                          "id": "rpc-0.0",
                          "type": "highlightElem",
                          "options": {
                            "targetElem": "0-0-0",
                            "highlightParent": true,
                            "text": {
                              "ru-RU": "Введите корректный номер счета",
                              "kz-KZ": "Шоттың нөмірін енгізіңіз",
                              "en-US": "Please enter an account number"
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              ]
            },
            {
              "id": "0-1",
              "type": "flat",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 16,
                  "right": 16,
                  "top": 8,
                  "bottom": 0
                }
              },
              "elements": [
                {
                  "id": "0-1-0",
                  "type": "label",
                  "options": {
                    "webTextAlignment": "left",
                    "mobileTextAlignment": "left",
                    "mobileDefaultConstraints": {
                      "left": 0,
                      "right": 0,
                      "top": 0,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Указан на бумажной квитанции, сумма к оплате мин 10 тг.",
                      "kz-KZ": "Қағаз түбіртегінде көрсетілген, сумма к оплате мин 10 тг.",
                      "en-US": "Indicated on paper receipt, сумма к оплате мин 10 тг."
                    },
                    "icon": "template_label_info",
                    "fontSize": 13
                  }
                }
              ]
            },
            {
              "id": "0-2",
              "type": "flat",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 0,
                  "right": 0,
                  "top": 16,
                  "bottom": 80
                }
              },
              "elements": [
                {
                  "id": "0-2-0",
                  "type": "submit",
                  "parameterId": "continue",
                  "actionId": "checkContract",
                  "options": {
                    "webTextAligment": "center",
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 16,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Проверить",
                      "kz-KZ": "Тексеру",
                      "en-US": "Check"
                    }
                  }
                }
              ]
            }
          ]
        }
      ]
    },
    {
      "id": "secondPage",
      "type": "normal",
      "options": {},
      "containers": [
        {
          "id": "1",
          "type": "flat",
          "options": {
            "mobileDefaultConstraints": {
              "left": 0,
              "right": 0,
              "top": 0,
              "bottom": 0
            }
          },
          "blocks": [
            {
              "id": "1-0",
              "type": "flat",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 0,
                  "right": 0,
                  "top": 16,
                  "bottom": 0
                },
                "mobileBackgroundColor": "FFFFFF"
              },
              "elements": [
                {
                  "id": "1-0-0",
                  "type": "flatList",
                  "options": {
                    "showUnderline": true,
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 8,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Информация",
                      "kz-KZ": "Ақпарат",
                      "en-US": "Information"
                    },
                    "listData": [
                      {
                        "id": "1-0-0-0",
                        "label": {
                          "ru-RU": "Номер лицевого счета",
                          "kz-KZ": "Шот нөмірі",
                          "en-US": "Account number"
                        },
                        "value": "{0}",
                        "dependencies": [
                          "contract"
                        ]
                      },
                      {
                        "id": "1-0-0-1",
                        "label": {
                          "ru-RU": "ФИО",
                          "kz-KZ": "Аты-жөні",
                          "en-US": "Account holder name"
                        },
                        "value": "{0}",
                        "dependencies": [
                          "clientName"
                        ]
                      },
                      {
                        "id": "1-0-0-2",
                        "label": {
                          "ru-RU": "Адрес",
                          "kz-KZ": "Мекен-жайы",
                          "en-US": "Account address"
                        },
                        "value": "{0}",
                        "dependencies": [
                          "clientAddress"
                        ]
                      },
                      {
                        "id": "1-0-0-3",
                        "label": {
                          "ru-RU": "Информация",
                          "kz-KZ": "Информация",
                          "en-US": "Information"
                        },
                        "value": "{0}",
                        "dependencies": [
                          "additionalInfo"
                        ]
                      }
                    ]
                  }
                },
                {
                  "id": "1-0-1",
                  "type": "getInfo",
                  "parameterId": "info",
                  "actionId": "hideAllErrors",
                  "options": {
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 2,
                      "bottom": 2
                    },
                    "description": {
                      "ru-RU": "Баланс",
                      "kz-KZ": "Баланс",
                      "en-US": "Balance"
                    },
                    "buttonText": {
                      "ru-RU": "Проверить",
                      "kz-KZ": "Тексеру",
                      "en-US": "Check"
                    },
                    "debtText": {
                      "ru-RU": "Задолженность на {hour:minute}",
                      "kz-KZ": "{hour:minute}-да Қарыз",
                      "en-US": "Debt on {hour:minute}"
                    },
                    "positiveText": {
                      "ru-RU": "Переплата на {hour:minute}",
                      "kz-KZ": "Переплата - {hour:minute}",
                      "en-US": "Balance on {hour:minute}"
                    }
                  }
                }
              ]
            },
            {
              "id": "1-1",
              "type": "roundedContainer",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 16,
                  "right": 16,
                  "top": 16,
                  "bottom": 0
                },
                "hintText": {
                  "ru-RU": "Сумма оплаты должна превышать 10 ₸",
                  "kz-KZ": "Төлем мөлшері 10 ₸ асу керек.",
                  "en-US": "Payment amount must be above 10 ₸"
                }
              },
              "elements": [
                {
                  "id": "1-1-0",
                  "parameterId": "amount",
                  "type": "inputField",
                  "options": {
                    "webAlignment": "Vertical",
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 2,
                      "bottom": 2
                    },
                    "description": {
                      "ru-RU": "Сумма к оплате",
                      "kz-KZ": "Төленетін сома",
                      "en-US": "Amount to pay"
                    },
                    "webPlaceholder": {
                      "ru-RU": "200",
                      "kz-KZ": "200",
                      "en-US": "200"
                    },
                    "formatting": "priceAmount",
                    "keyboard": "decimal",
                    "onResignPreCheck": {
                      "parameterId": "amount",
                      "checkCondition": {
                        "range": {
                          "min": "9",
                          "max": "1000000000",
                          "dataType": "double"
                        }
                      },
                      "errorActions": [
                        {
                          "id": "rpc-1.0",
                          "type": "highlightElem",
                          "options": {
                            "targetElem": "1-1-0",
                            "highlightParent": true,
                            "text": {
                              "ru-RU": "Введите сумму, минимум 10 тг.",
                              "kz-KZ": "Соманы енгізіңіз, минимум 10 тг.",
                              "en-US": "Please enter the amount, min 10 tg."
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              ]
            },
            {
              "id": "1-2",
              "type": "flat",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 0,
                  "right": 0,
                  "top": 16,
                  "bottom": 0
                }
              },
              "elements": [
                {
                  "id": "1-2-0",
                  "parameterId": "commission",
                  "actionId": "commissionAlert",
                  "type": "commission",
                  "options": {
                    "calculateFromParamId": "amount",
                    "webTextAligment": "left",
                    "mobileTextAlignment": "center",
                    "mobileDefaultConstraints": {
                      "left": 0,
                      "right": 0,
                      "top": 0,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Комиссия: {0} ₸",
                      "kz-KZ": "Комиссия: {0} ₸",
                      "en-US": "commission: {0} ₸"
                    },
                    "highLightedText": [
                      {
                        "ru-RU": "{0} ₸",
                        "kz-KZ": "{0} ₸",
                        "en-US": "{0} ₸"
                      }
                    ],
                    "dependencies": [
                      "commission"
                    ]
                  }
                }
              ]
            },
            {
              "id": "1-3",
              "type": "roundedContainer",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 16,
                  "right": 16,
                  "top": 16,
                  "bottom": 0
                }
              },
              "elements": [
                {
                  "id": "1-3-0",
                  "parameterId": "card",
                  "type": "paymentSelector",
                  "options": {
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 2,
                      "bottom": 2
                    },
                    "label": {
                      "ru-RU": "Выберите способ оплаты",
                      "kz-KZ": "Төлем әдісін таңдаңыз",
                      "en-US": "Choose a payment method"
                    }
                  }
                }
              ]
            },
            {
              "id": "1-4",
              "type": "flat",
              "options": {
                "webAlignment": "Vertical",
                "mobileDefaultConstraints": {
                  "left": 0,
                  "right": 0,
                  "top": 16,
                  "bottom": 80
                }
              },
              "elements": [
                {
                  "id": "1-4-0",
                  "type": "submit",
                  "actionId": "showExecPage",
                  "parameterId": "overallAmount",
                  "options": {
                    "webTextAligment": "center",
                    "mobileDefaultConstraints": {
                      "left": 16,
                      "right": 16,
                      "top": 16,
                      "bottom": 0
                    },
                    "description": {
                      "ru-RU": "Оплатить {0} ₸",
                      "kz-KZ": "Төлеу үшін {0} ₸",
                      "en-US": "Pay {0} ₸"
                    },
                    "dependencies": [
                      "overallAmount"
                    ],
                    "sumFromParams": [
                      "amount",
                      "commission"
                    ]
                  }
                }
              ]
            }
          ]
        }
      ]
    },
    {
      "id": "execPage",
      "type": "execPage",
      "options": {
        "actionId": "exec",
        "displayInfo": {
          "card": "card",
          "commission": "commission",
          "amount": "amount"
        }
      },
      "containers": []
    }
  ]
```
Компоненты внутри страницы типа **normal** конструируются рекурсивно из массива blocks внутри containers.
Моделька для страницы:

```swift
struct Page: Codable {
        var id: String!
        var type: PageType!
        var options: PageOptions!
        var containers: [Container]!

        enum PageType: String, Codable {
            case normal
            case execPage
            case noInvoicePage
        }

        struct PageOptions: Codable {
            var initActions: [String]?
            var actionId: String?
            var displayInfo: DisplayInfo?
            var mobileBackgroundColor: String?

            struct DisplayInfo: Codable {
                var commission: String!
                var card: String!
                var amount: String!
                var contract: String?
            }
        }
    }
```

## id
id страницы в шаблоне. Используется в качестве идентификатора при переходах. Так же используется action-ом **showPage**
```swift
var id: String!
```

## type
На данный моментт есть есть три типа страниц.
```swift
  case normal
    case execPage
    case noInvoicePage
```
### normal
Используется при отрисовки всех главных экранов, рисуется рекурсивно. Внутри находятся контейнеры, компоненты, блоки и так далее.
### execPage
Используется в тот момент когда нужно открыть хардкодно сделанную страницу оплаты с определенными параметрами из displayInfo в котором находится карта сумма и коммиссия
### noInvoicePage
Фоллбек страница, открывается в том случае если инвойсы к определенному контракту пришли пустые (нечего оплачивать).
Использует displayInfo в котором находится номер контракта.

## mobileBackgroundColor
Имеет в себе hex-овое значение цвета. Описывает background цвет всей страницы. При смене дизайна можно заменить везде на бэкенде.
```json
"mobileBackgroundColor": "FFFFFF"
```
Использование:
```swift
let output = viewModel.transform(input: input)
output.drawView
            .subscribe(onNext: { [weak self] (page) in
                guard let self = self else { return }
                if let color = page.options.mobileBackgroundColor {
                    self.view.backgroundColor = UIColor.colorWithHexString(color)
                } else {
                    self.view.backgroundColor = HBColors.paymentBackground.color
                }
                self.addTemplateView(view: self.createView(from: page))
            })
            .disposed(by: disposeBag)

```
# Вопросы
$$SprintSquads + \emptyset  $$

>  Если у вас будут вопросы при имплементации и дополнению шаблона вы всегда можете обратиться с вопросом к yerbol@sprintsquads.com либо отправить вопрос на сайте sprintsquads.com